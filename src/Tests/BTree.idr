module Tests.BTree

import BTree
import Test.Unit.Assertions


testInsertMember : IO ()
testInsertMember =
  let tree = insert empty 1 in
  let tree = insert tree 5 in
  let tree = insert tree 3 in
  do
    assertTrue $ member tree 1
    assertTrue $ member tree 5
    assertTrue $ member tree 3
    pure ()

testNotInsertMember : IO ()
testNotInsertMember =
  let tree = insert empty 1 in
  let tree = insert tree 5 in
  let tree = insert tree 3 in
  do
    assertFalse $ member tree 2
    assertFalse $ member tree 4
    pure ()

testPopMax : IO ()
testPopMax =
  let tree = insert empty 1 in
  let tree = insert tree 5 in
  let tree = insert tree 3 in
  do
    let (tree, max) = popMax tree
    assertEquals max (Just 5)
    let (tree, max) = popMax tree
    assertEquals max (Just 3)
    let (tree, max) = popMax tree
    assertEquals max (Just 1)
    let (tree, max) = popMax tree
    assertEquals max Nothing
    pure ()

testPopMin : IO ()
testPopMin =
  let tree = insert empty 1 in
  let tree = insert tree 5 in
  let tree = insert tree 3 in
  do
    let (tree, min) = popMin tree
    assertEquals min (Just 1)
    let (tree, min) = popMin tree
    assertEquals min (Just 3)
    let (tree, min) = popMin tree
    assertEquals min (Just 5)
    let (tree, min) = popMin tree
    assertEquals min Nothing
    pure ()

testDelete : IO ()
testDelete =
  let tree = insert (the (BTree Integer) empty) 1 in
  let tree = insert tree 5 in
  let tree = insert tree 3 in
  do
    assertTrue $ member tree 1
    let tree = delete tree 1
    assertFalse $ member tree 1
    pure ()



testToTreeMember : IO ()
testToTreeMember =
  let tree = toTree [1, 5, 3] in
  do
    assertTrue $ member tree 1
    assertTrue $ member tree 5
    assertTrue $ member tree 3
    pure ()

testToTreeToList : IO ()
testToTreeToList =
  let tree = toTree [1, 5, 3] in
  let list = toList tree in
  do
    assertEquals list [1, 3, 5]
    pure ()

testMax : IO ()
testMax =
  let tree = toTree [1, 4, 3, 5, 2] in
  do
    assertEquals (max tree) (Just 5)
    pure ()

testMin : IO ()
testMin =
  let tree = toTree [1, 4, 3, 5, 2] in
  do
    assertEquals (min tree) (Just 1)
    pure ()

testSplit1 : IO ()
testSplit1 =
  let tree = toTree [1, 5, 3, 2, 4] in
  let (l, v ,r) = split tree 3 in
  do
    assertEquals (toList l) [1, 2]
    assertEquals v (Just 3)
    assertEquals (toList r) [4, 5]
    pure ()

testSplit2 : IO ()
testSplit2 =
  let tree = toTree [1, 5, 2, 4] in
  let (l, v ,r) = split tree 3 in
  do
    assertEquals (toList l) [1, 2]
    assertEquals v Nothing
    assertEquals (toList r) [4, 5]
    pure ()

testSplit3 : IO ()
testSplit3 =
  let tree = toTree [1, 5, 2, 4] in
  let (l, v ,r) = split tree 6 in
  do
    assertEquals (toList l) [1, 2, 4, 5]
    assertEquals v Nothing
    assertEquals (toList r) []
    pure ()

testSplit4 : IO ()
testSplit4 =
  let tree = toTree [1, 5, 2, 4] in
  let (l, v ,r) = split tree 0 in
  do
    assertEquals (toList l) []
    assertEquals v Nothing
    assertEquals (toList r) [1, 2, 4, 5]
    pure ()


testSplit : IO ()
testSplit = do
  testSplit1
  testSplit2
  testSplit3
  testSplit4


testUnion : IO ()
testUnion =
  let tree1 = toTree [1, 3, 5] in
  let tree2 = toTree [2, 3, 4] in
  let tree = union tree1 tree2 in
  do
    assertEquals (toList tree) [1, 2, 3, 4, 5]
    pure ()


export
test : IO ()
test = do
  testInsertMember
  testNotInsertMember
  testPopMax
  testPopMin
  testDelete
  testToTreeMember
  testToTreeToList
  testMax
  testMin
  testSplit
  testUnion
