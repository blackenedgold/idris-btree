module BTree

import Prelude.Algebra

export
data BTree: (a : Type) -> Type where
  Leaf : BTree a
  Node : (l: BTree a) -> (v: a) -> (r: BTree a) -> BTree a

%name BTree tree, tree1, tree2

export
empty: BTree a
empty = Leaf

export
insert: Ord a => BTree a ->  a -> BTree a
insert Leaf x = Node Leaf x Leaf
insert (Node l v r) x = case compare x v of
  LT => Node (insert l x) v r
  EQ => Node         l    v r
  GT => Node         l    v (insert r x)


export
member : Ord a => BTree a -> a -> Bool
member Leaf x = False
member (Node l v r) x = case compare x v of
  LT => member l x
  EQ => True
  GT => member r x


export
max: BTree a -> Maybe a
max Leaf = Nothing
max (Node _ v Leaf) = Just v
max (Node _ _ r) = max r

export
min: BTree a -> Maybe a
min Leaf = Nothing
min (Node Leaf v _) = Just v
min (Node l _ _) = min l

export
popMax : BTree a -> (BTree a, Maybe a)
popMax Leaf = (Leaf, Nothing)
popMax (Node l v Leaf) = (l, Just v)
popMax (Node l v r) = let (r', max) = popMax r in
                         (Node l v r', max)

export
popMin : BTree a -> (BTree a, Maybe a)
popMin Leaf = (Leaf, Nothing)
popMin (Node Leaf v r) = (r, Just v)
popMin (Node l v r) = let (l', min) = popMin l in
                         (Node l' v r, min)

export
delete : Ord a => BTree a -> a -> BTree a
delete Leaf x = Leaf
delete (Node l v r) x = case compare x v of
  LT => Node (delete l x)  v r
  GT => Node l v (delete r x)
  EQ => case popMax l of
        (l', Just max) => Node l' max r
        (l', Nothing) => r -- l' = Leaf

export
fold: BTree a -> b -> (b -> a -> b -> b) -> b
fold  Leaf  x f = x
fold  (Node l v r)  x f = f (fold  l x f) v (fold r x f)

export
size : BTree a -> Integer
size tree = fold tree 0 (\l, _, r => l + 1 + r)


export
maxHeight : BTree a -> Integer
maxHeight t = fold t 0 (\lh,_,rh => (max lh rh) + 1)

export
minHeight : BTree a -> Integer
minHeight t = fold t 0 (\lh,_,rh => (min lh rh) + 1)

export
height : BTree a -> Integer
height = maxHeight


-- --   a
-- --  / \
-- -- l  b
-- --   / \
-- --  rl  rr
-- --
-- --  |
-- --  v
-- --
-- --     b
-- --    / \
-- --   a   r
-- --  / \
-- -- l  rl
-- rotateLeft: Ord a => BTree a -> BTree a
-- rotateLeft Leaf = Leaf
-- rotateLeft (Node l a Leaf) = Node l a Leaf
-- rotateLeft (Node l a (Node rl b rr)) = Node (Node l a rl) b rr

-- --     b
-- --    / \
-- --   a   r
-- --  / \
-- -- ll lr
-- --
-- --  |
-- --  v
-- --
-- --   a
-- --  / \
-- -- ll b
-- --   / \
-- --  lr  r

-- rotateRight: Ord a => BTree a -> BTree a
-- rotateRight Leaf = Leaf
-- rotateRight (Node Leaf v r) = Node Leaf v r
-- rotateRight (Node (Node ll a lr) b r) = Node ll a (Node lr b r)

Foldable BTree where
  foldr f init Leaf = init
  foldr f init (Node l v r) =
    let r = foldr f init r in
    let v = f v r in
    foldr f v l
  foldl f init Leaf = init
  foldl f init (Node l v r) =
    let l = foldl f init l in
    let v = f l v in
    foldl f v r


export
toList: BTree a -> List a
toList tree = foldr (::) [] tree

export
toTree: Ord a => List a -> BTree a
toTree xs = foldl insert empty xs


export
split: Ord a => BTree a -> a -> (BTree a, Maybe a, BTree a)
split Leaf x = (Leaf, Nothing, Leaf)
split (Node l v r) x = case compare x v of
  LT => let (ll, lv, lr) = split l x in (ll, lv, Node lr v r)
  EQ => (l, Just v, r)
  GT => let (rl, rv, rr) = split r x in (Node l v rl, rv, rr)



export
union : Ord a => BTree a -> BTree a -> BTree a
union tree1 tree2 = foldl insert tree1 tree2

export
intersection : Ord a => BTree a -> BTree a -> BTree a
intersection tree1 tree2 = foldl (\acc,elm => if member tree1 elm
                                              then insert acc elm
                                              else acc)
                                 empty tree2

export
difference : Ord a => BTree a -> BTree a -> BTree a
difference tree1 tree2 = foldl delete tree1 tree2


Ord a => Semigroup (BTree a) where
  (<+>) = union

Ord a => Monoid (BTree a) where
  neutral = empty
